/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import Vue from 'vue';
import NotifyPlugin from '~/core/client/notify';

Vue.use(NotifyPlugin);
