/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import axios from '~/plugins/axios';
import Api from './Api';

export default class Domains extends Api {
  constructor() {
    super();
    this.url = super.fullUrl(`/domains`);
  }

  getList() {
    return axios.get(`${this.url}/getList`, super.defaultOpts)
      .then(super.response);
  }

  openDir(data) {
    return axios.post(`${this.url}/open-dir`, data, super.defaultOpts)
      .then(super.response);
  }

  save(data) {
    return axios.post(`${this.url}/save`, data, super.defaultOpts)
      .then(super.response);
  }

  remove(name) {
    return axios.delete(`${this.url}/remove/${name}`, super.defaultOpts)
      .then(super.response);
  }
}
