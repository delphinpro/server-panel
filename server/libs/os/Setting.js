/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import _ from 'lodash';
import OpenServer from "./index";
import Database from '../../db';
import {isJson} from '../../utils';
import Init from './Init';
import Profile from './Profile';

export default class Setting extends OpenServer {
  constructor() {
    super();
  }

  getData() {
    let obj = {};
    return Database.Setting.all().then(list => {

      list.forEach(({key, value}) => {
        obj[key] = isJson(value) ? JSON.parse(value) : value;
      });

      return obj;
    });
  }

  async save(data = {}) {
    const saveKey = (obj) => {
      return Database.upsert(Database.Setting, obj, {key: obj.key});
    };

    let arr = [];
    for (let k in data) {
      arr.push(saveKey({
        key: k,
        value: _.isObject(data[k]) ? JSON.stringify(data[k]) : data[k]
      }))
    }

    return new Promise(async (resolve, reject) => {
      Promise.all(arr).catch(reject); // сохраняем данные в свою БД

      const boolToStr = (val) => {
        return String(Number(val));
      };

      const $init = new Init();
      let init = await $init.getData();

      const $profile = new Profile(init.main.profile);
      let profile = await $profile.getData();

      console.log('init');
      console.log(init);

      console.log('profile');
      console.log(profile);

      init.main = Object.assign(init.main, {
        autostart: boolToStr(data.general.autostart),
        clearlogs: boolToStr(data.general.clearlogs),
        checkupdate: boolToStr(data.general.checkupdate),
        wait: String(data.general.wait)
      });

      profile.main = Object.assign(profile.main, {
        allow: boolToStr(data.server.allow),
        astart: boolToStr(data.server.astart),
        debugmode: boolToStr(data.server.debugmode),
        selfhosts: boolToStr(data.server.selfhosts)
      });

      profile.ftp = Object.assign(profile.ftp, {
        ftp: boolToStr(data.server.ftp.isStartServer),
        ftpcommandtimeout: String(data.server.ftp.commandtimeout),
        ftpconnecttimeout: String(data.server.ftp.connecttimeout)
      });
      profile.ports = Object.assign(profile.ports, {
        mysqlport: P.MySQL,
        postgresqlport: P.Postgres,
        mongodbport: P.MongoDB,
        httpport: P.HTTP,
        httpsport: P.HTTPS,
        httpbackport: P.Backend,
        ftpport: P.FTP,
        sftpport: P.FTPS,
        phpport: P.PHP,
        redisport: P.Redis,
        memcacheport: P.Memcache
      });

      await $init.saveData(init);
      await $profile.saveData(profile);

      resolve();

      // перезагрузка OS
      OpenServer.restart();
    });
  }
}
