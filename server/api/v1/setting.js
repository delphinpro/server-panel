import {Router} from 'express';
import Setting from '../../libs/os/Setting';

const router = Router();
const MU = '/setting';

router.get(`${MU}/getData`, (req, res) => {
  (new Setting()).getData()
    .then(data => res.json(data))
    .catch(e => res.sendStatus(500));
});

router.post(`${MU}/save`, (req, res) => {
  (new Setting()).save(req.body)
    .then(() => res.sendStatus(201))
    .catch(e => {
      console.error(e);
      res.sendStatus(500)
    });
});

export default router;
