/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import path from 'path';
import {EventEmitter} from 'events';
import mosca from 'mosca';
import Fs from '../libs/Fs';

const noop = () => {
};

export default class MqttServer extends EventEmitter {
  constructor() {
    super();
    this.store = path.resolve(DATA_DIR, 'mqtt');
    this._init();
  }

  async _init() {
    await Fs.emptyDir(this.store);
  }

  listen(port = 1883, cb = noop) {
    const server = new mosca.Server({
      type: "mqtt",
      allowNonSecure: true,
      host: '127.0.0.1',
      port
    });

    const db = new mosca.persistence.LevelUp({
      path: this.store
    });
    db.wire(server);

    server.on('ready', () => {
      this.emit('ready', port);
      cb();
    });

    return this;
  }
}
