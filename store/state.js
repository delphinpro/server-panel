/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

'use strict';

const state = {
  locales: ['ru', 'en'],
  locale: 'ru',
  localeListFull: {
    ru: 'Русский',
    en: 'English'
  },
  isLaunch: false,
  requireRestartOS: false,
  ENV: {
    VALID_ROOT_DIR: true
  }
};

export default state;
