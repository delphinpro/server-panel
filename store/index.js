/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

'use strict';

// import Vue from 'vue';
import Vuex from 'vuex';
// import UrlLocalePlugin from '~/plugins/get-url';
// import UrlPartPlugin from '~/plugins/Url';
// import Permission from '~/core/permission';

import state from './state';
import actions from './actions';
import mutations from './mutations';

// Vue.use(UrlLocalePlugin);
// Vue.use(UrlPartPlugin);
// Vue.use(Permission);

const createStore = () => {
  return new Vuex.Store({
    state,
    actions,
    mutations
  })
};

export default createStore;

if (process.browser) {
  let heightT = null;
  let bodyHeight = () => {
    let MH = document.querySelector('.top-header');
    let MO = document.querySelector('.modal-open');
    // let FH = document.querySelector('footer');
    if (MH) {
      let H = MH.clientHeight;
      let F = 0; // FH.clientHeight;
      let mo = MO ? 45 : 0;
      document.querySelector('.left-sidebar').style.height = window.innerHeight + mo - (H + F) + 'px';
      document.querySelector('.main-content').style.height = window.innerHeight + mo - (H + F) + 'px';
    }
  };

  if (heightT) {
    clearInterval(heightT);
  }

  heightT = setInterval(bodyHeight, 500);
  window.addEventListener('resize', bodyHeight, false);
}
